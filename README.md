Ms computes an interval matrix enclosing W_k(A), where A is an n x n matrix. If successful, it is proved that A has no eigenvalues on the branch cuts. 
If successful, the uniqueness is proved. The Symbolic Math Toolbox, INTLAB, and cond_check.m are required for the execution. 


Mj computes an interval matrix enclosing W_k(A), where A: an n x n matrix. If successful, it is proved that A has no eigenvalues on the branch cuts. 
If successful, the uniqueness is proved. The INTLAB, lambertwmatrix.m, and cond_check.m are required for the execution.


cond_check verifies Condition k for the pair (x,r) of vectors.


verLambertW computes an interval enclosing W_k(x), where x is a complex number. The Symbolic Math Toolbox, and INTLAB are required for the execution.